/**
ALL codes are programmed by Nana Okamoto;
 */
import java.awt.event.*;

//control class
public class Key  extends KeyAdapter {
    static boolean left, right, up, down, space, enter;

// this method is used when a key is used
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT: left = true; break;	    // left
            case KeyEvent.VK_RIGHT: right = true; break;	// right
            case KeyEvent.VK_UP: up  = true; break;	       // up
            case KeyEvent.VK_DOWN: down = true; break;	// down
            case KeyEvent.VK_SPACE: space = true; break;	// SPACE
            case KeyEvent.VK_ENTER: enter = true; break;	// Enter
        }
    }

// this method is used when a key is released
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT: left = false; break;	// left
            case KeyEvent.VK_RIGHT: right = false; break;	// right
            case KeyEvent.VK_UP: up  = false; break;	       // up
            case KeyEvent.VK_DOWN: down = false; break;	// down
            case KeyEvent.VK_SPACE: space = false; break;	// SPACE
            case KeyEvent.VK_ENTER: enter = false; break;	// Enter
        }
    }
}
