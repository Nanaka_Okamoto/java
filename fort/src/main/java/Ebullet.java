import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Nana Okamoto
 * Date: 12/11/16
 * Time: 22:06
 * University Java Project - Fort!
 * Group members: Hristo Dimitrov, Nana Okamoto, Paul Clydesdale, Danne Connolly;
 */
public class Ebullet {

    private Fort app;				// applet
    private Image img;					// picture
    public int x, y;					// position
    private int vx, vy;				// speed
    private boolean appear;			// flag if the Bullet is already on the display

    // constructor
    public Ebullet(Fort applet) {
        app = applet;
        appear = false;
//        img = Toolkit.getDefaultToolkit().createImage(Bullet.class.getResource("bullet.gif"));                            // read picture file
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\bullet.png");                          // TODO find dynamic solution
    }

    // shoot the Bullet
    void shoot(int x, int y, int vx, int vy)	{
        if (!appear) {
            this.x = x;
            this.y = y;
            this.vx = vx;
            this.vy = vy;
            appear = true;
        }
    }

    // move
    void move() {
        if (appear) {
            x += vx;
            y -= vy;
            if (x < -8 || x > app.getw() || y < -8 || y > app.geth())              //if out of the window
                appear = false;
        }
    }

    // draw the Bullet
    void draw(Graphics g) {
        if (appear&& app.show) g.drawImage(img, x, y, app);
    }
}
