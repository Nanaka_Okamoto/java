/**

 */
import java.awt.*;
import javax.swing.*;

public class Fort extends JPanel implements Runnable  {
    private Player player;						// player object
    private Enemy[] enemy = new Enemy[5];// enemy object
    private Enemy2[] enemy2 = new Enemy2[5]; //enemy2 object
    private volatile Thread gameThread;	// game thread
    private Graphics g;					//graphic handler
    private int width, height;			// applet width height
    private Background back;
    private Boss boss;
    private int pbx;            // player bullet position   x
    private int pby;             //player bullet position y
    private int ebx;            //enemy bullet position x
    private int eby;            //enemy bullet position y
    private int bbx; //boss bullet position x
    private int bby; //boss bullet position y
    public int score;      //player score
    public int hitPlayer;          //hit counter for player
    public int hitEnemy;    //hit counter for enemy
    public int scene;      //scene: 0.start  1.gaming  2.Not clear  3.level clear   4. game over 5. Game clear
    boolean[] alreadyHit = new boolean[5];     //used for player getting hit by enemy2 type
    boolean[] alreadyHit2 = new boolean[5];     //used for player getting hit by enemy2 type
    boolean alreadyHitB;     //used for player getting hit by Boss type
    boolean[] alreadyHitBB = new boolean[10];   //used for player getting hit by boss bullet
    boolean alreadySet;    //used for  level increment
    boolean alreadyDead;  //used for life decrement
    boolean[] keepHitFlag = new boolean[5]; //used to keep the enemy hit stay true for gif effect
    boolean[] keepHit2Flag = new boolean[5]; //used to keep the enemy2 hit stay true for gif effect
    boolean keepHitBFlag; //used to keep the boss hit stay true for gif effect
    int[] timerEnemyHit = new int[5]; // used for keepHitFlag
    int[] timerEnemy2Hit = new int[5]; // used for keepHi2tFlag
    int timerBossHit ; // used for keepHitBFlag
    public int level;    //levels 1 2 3 4 5 6 7
    public boolean show;   //used to make sure that objects are drawn only when needed
    public int life ;
    public int previous_score; //score of previous level
    public int enemyIndex;      // enemy's array's index used which enemy is currently checked
    public int enemyIndex2; 
    public int clearscore;      //score to clear each level
    public int invinsibleTimer; // used for player to stay okay from the minion attack after hit
    public boolean alreadyhit;
    boolean keephitflag;
    int timerplayer;
// initialization
    public void init() {
        
    alreadyhit = false;
     keephitflag = false;
     timerplayer= 0;
        
        width = 300;                                   //getting the applet size
        height = 400;
        back =  new Background(this);
        addKeyListener(new Key());                     // key listener
        requestFocus();                                // request focus
        player = new Player(this);   //player object created
        boss = new Boss(this);
        for(int i=0; i<5; i++ ){
            enemy[i] = new Enemy(this, i);                //enemy objects created
            enemy2[i] = new Enemy2(this, i);
        }
       
        //initialization of all nessary paramators in use
        show = false;
        alreadySet = false;
        alreadyDead = false;
        
        for(int i=0; i<5; i++ ){
            alreadyHit[i] = false;
            alreadyHit2[i] = false;
            
            keepHitFlag[i] =false;
            keepHit2Flag[i] =false;
            
            timerEnemyHit[i] = 0; 
            timerEnemy2Hit[i] = 0; 
        }
         alreadyHitB = false;
         keepHitBFlag =false;
         timerBossHit = 0; 
         invinsibleTimer = 100;
        keepHitBFlag =false;
        timerBossHit = 0; 
       for(int i = 0; i <10; i++){
           alreadyHitBB[i] = false;

        }
        
        level = 1;
        life = 5;
        clearscore = 100;
    }

// start game thread
    public void start() {
        if(gameThread == null) {
            gameThread = new Thread(this);
            gameThread.start();
        }
    }

// stop game thread
    public void stop() {
        gameThread = null;
    }

//run the game
    public void run() {
        while (true) {                          //gameThread == Thread.currentThread()
            repaint();                          // re-draw
            try {                               //waining 20 seconds
                Thread.sleep(19);
            } catch (InterruptedException e) {
                break;
            }
        }
    }

//draw
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        back.run(g);              //background start!!!

        if(scene == 0 || (Key.enter && scene == 4 )){    //title display
            show = false;
            if(scene == 4) {
                scene = 0;
                score = 0;
                hitEnemy = 0;
                hitPlayer = 0;
                life = 5;
            }
            g.setColor(Color.white);
            g.setFont(new Font("Arial", Font.BOLD, 28));
            g.drawString("Rainbow Shooter!", 25, 180);
            g.setFont(new Font("Arial", Font.PLAIN, 20));
            g.drawString("HIT ENTER KEY.", 70, 350);
        }//gaming
        if( (Key.enter && scene == 0 ) || scene ==1 || 
                (Key.enter && scene == 3 )|| (Key.enter && scene == 2 )){
            if(scene == 1){
                show = true;
            }
            else{
                show = false;
            }

             player.run(g);          //player start!!!!!
             //enemy objects differ when to be used depending on each level..
             if(level == 1){//two same enemy comes without any bullets
                 enemy[0].run(g);          //enemies start!!!
                 enemy[1].run(g);
             }
             if(level == 2){//two normal enemy one bullet enemy 
                 enemy[0].run(g);          //normal enemies start!!!
                 enemy[1].run(g);           
                 enemy[2].run(g);          //enemy with bullet
             }
             if(level == 3){//one normal enemy and one bullet enemy and one enemy2 
                 enemy[0].run(g);          
                 enemy[1].run(g); 
                 enemy2[0].run(g);  
             }
             if(level == 4){
                 enemy2[0].run(g); 
                 enemy2[1].run(g);
                 enemy2[3].run(g); 
                 enemy[2].run(g);
                 
             }
             if(level == 5){//two bullets enemy and 2 enemy2 
                 enemy[3].run(g);          
                 enemy[1].run(g); 
                 enemy2[0].run(g); 
                 enemy2[1].run(g); 
             }
             if(level == 6){//boss class is debugged
                 boss.run(g);          //boss start!!!
                 invinsibleTimer++;
             }

            if(scene == 2)  {
                score = previous_score;
                hitEnemy = 0;
                hitPlayer = 0;
                scene = 1;
                alreadyDead = false;
            }
            if(scene == 3)   {
                if(!alreadySet){
                    alreadySet = true;
                    level++;
                    score = 0;
                    //previous_score = score;
                }
                hitEnemy = 0;
                hitPlayer = 0;
                scene = 1;
                alreadySet = false;
            }

            scene = 1;
            if(level == 6){
                clearscore = 1000;
            }

            g.setColor(Color.white);
            g.drawString("SCORE : " + String.valueOf(score+ "/" +clearscore), 10, 20);
            g.drawString("Damage : " + String.valueOf(hitPlayer + "/" +5), 130, 20);
            g.drawString("Life : " + String.valueOf(life + "/" +5), 230, 20);
            
        }
        if(hitPlayer >= 5 || scene == 2 || scene == 4){ //Player dead  or game over
            show =false; 
            if(scene != 4)  {
                scene = 2;
            }
            if(!alreadyDead){
                if(life >0) {
                    life--;
                }
                else {
                    scene = 4;
                }
                alreadyDead = true;
            }
            g.setColor(Color.white);
            g.setFont(new Font("Arial", Font.BOLD, 28));
            if(scene == 2)
                g.drawString("TRY AGAIN", 64, 180);
            if(scene == 4)
                g.drawString("GAME OVER", 64, 180);
        }
        if((score >= clearscore && level != 6) || scene == 3 )  {   //level clear
            scene = 3;
            if(level == 1){
                clearscore = 100;
            }
            if(level == 2){
                clearscore = 200;
            }
             if(level == 3){
                clearscore = 200;
            }
            if(level == 4){
                clearscore = 200;
            }
             if(level == 5){
                clearscore = 200;
            }

            hitEnemy = 0;
            g.setColor(Color.white);
            g.setFont(new Font("Arial", Font.BOLD, 28));
            g.drawString("Level Cleared", 64, 180);
        }
        if((score >=1000 && level == 6 )|| scene == 5){//change back to boss level 6 later
             scene = 5;
            g.setColor(Color.white);
            g.setFont(new Font("Arial", Font.BOLD, 28));
            g.drawString("Game Cleared", 64, 180);

        }

    }

// update
    public void update(Graphics g) {
        paint(g);
    }

// applets' get size
    int getw() {return width;}
    int geth() {return height;}
    
    
    //get player's current position used for boss class
    public int getPlayerX(){
        int x = player.x; 
        return x;
    }
    //get player's current position used for boss class
    public int getPlayerY(){
        int y = player.y; 
        return y;
    }


    //return true if player is hit with enemy
    public boolean getHitFlagPlayer(int i) {
        ebx = enemy[i].getXbullet();
        eby = enemy[i].getYbullet();
        //case player get hit by enemy
        if(player.x + 32 > enemy[i].x && player.x < enemy[i].x + 32 &&
                     player.y + 32 > enemy[i].y && player.y < enemy[i].y + 32){
          if(!alreadyHit[i]) {
             hitPlayer++;
             alreadyHit[i] = true;
           }
           return true;
        }
        //case player gets hit by enemy2
         else if(player.x + 32 > enemy2[i].x && player.x < enemy2[i].x + 32 &&
                     player.y + 32 > enemy2[i].y && player.y < enemy2[i].y + 32){
            if(!alreadyHit[i]) {
               hitPlayer++;
               alreadyHit[i] = true;
            }
            return true;
        }
        //case player get hit by enemy bullet
        else if(ebx + 8 > player.x && ebx < player.x + 32 &&
                     eby +8 > player.y  && eby < player.y + 32){
             if(!alreadyHit[i]) {
               hitPlayer++;
               alreadyHit[i] = true;
             }
             return true;
        }
        else{
             alreadyHit[i] = false;
              return false;
        }
   
        
           
    }
    //return true if enemy is hit with player bullet
    public boolean getHitFlagEnemy(int index){
        pbx = player.getXbullet();
        pby = player.getYbullet();
        if(pbx + 8 > enemy[index].x && pbx < enemy[index].x + 32 &&
                pby +8 > enemy[index].y  && pby < enemy[index].y +32 ){
           if(!keepHitFlag[index]){
              //index 0, 1: normal index 2, 3 bullet index 4 spare
              //index 1,3 :speed quicker
              if((index == 0 || index == 4)){
                    score = score + 5;
               }
               if(index == 1){
                   score = score + 8;
               }
               if(index == 2 ){
                   score = score +10;
               }
                if(index == 3 ){
                   score = score +20;
               }

               hitEnemy++;
               timerEnemyHit[index]=0; 
           }
             keepHitFlag[index] = true;
           return true;
       }
       else{
           if(timerEnemyHit[index] <10 ){
               timerEnemyHit[index]++;
               return true;
           }
           else{
                 keepHitFlag[index] = false;
                 return false; 
           }

       }
    }
    
   //return true if enemy2 is hit with player bullet
    boolean getHitFlagEnemy2(int index) {
                pbx = player.getXbullet();
        pby = player.getYbullet();
       // while(enemyIndex < 5){
            if(pbx + 8 > enemy2[index].x && pbx < enemy2[index].x + 32 &&
                    pby +8 > enemy2[index].y  && pby < enemy2[index].y +32 ){
               if(!keepHit2Flag[index]){
                    //index 0, 1: normal index 2, 3 bullet index 4 spare
                    //index 1,3 :speed quicker
                   if(index == 0 || index == 4){
                        score = score + 7;
                   }
                   if(index == 1){
                       score = score + 8;
                   }
                   if(index == 2 ){
                       score = score +10;
                   }
                    if(index == 3 ){
                       score = score +20;
                   }
                   hitEnemy++;
                   timerEnemy2Hit[index]=0; 
               }
                keepHit2Flag[index]= true;
               return true;
           }
           else{
               while(timerEnemy2Hit[index] <10 ){
                   timerEnemy2Hit[index]++;
                   return true;
               }
               keepHit2Flag[index] = false;
               return false;
           }
    }
    
    //return true if boss hits the player
    public boolean getHitFlagPlayer_Boss(){
            //case player get hit by boss
            if(player.x + 32 > boss.x && player.x < boss.x + 120 &&
                     player.y + 32 > boss.y && player.y < boss.y + 120){
                if(!alreadyHitB) {
                   hitPlayer++;
                   alreadyHitB = true;
                }
                return true;
            }
              //not hit 
          else{
                  alreadyHitB= false;
                  return false;
               }
  
    }
    
    //return true if each boss minion hit the player
    /*index tells which minion is hitting or not */
    public boolean getHitFlagPlayer_BBllet(int index){
        bbx = boss.getXbullet(index);
        bby = boss.getYbullet(index);
        //case player get hit by boss minion bullets
         if(bbx + 32 > player.x && bbx < player.x + 32 && bby +32 > player.y  && bby < player.y + 32){
            if(!alreadyhit) {//and you have not hit by another bullet yet
                if(timerplayer >10){
                    hitPlayer++;
                     alreadyhit = true; 
                    timerplayer = 0;
                    return true;
                } 
                else{
                     timerplayer++;
                     alreadyhit = true; 
                     return false;
                }  
            }
            else{ //you have already hit by another bullet
                timerplayer++;
                alreadyhit=false;
                return false;
            }
         }
         else{ //if you are not hit
             alreadyhit = false;
             return false; 
         } 
     }  
    
    //return true if boss is hit with player bullet
    public boolean getHitFlagBoss(){
        //get bullet position of the player
        if(level == 6){
            pbx = player.getXbullet();
            pby = player.getYbullet();
            if(pbx + 8 > boss.x && pbx < boss.x + 120 &&
                pby +8 > boss.y  && pby < boss.y +120 ){
                   if(!keepHitBFlag){
                       hitEnemy++;
                       if(level == 6){//thi 
                             score = score + 10; 
                       }

                       timerBossHit=0; 
                   }
                   keepHitBFlag = true;
                   return true;
               }
              else{
                  while(timerBossHit <10 ){
                      timerBossHit++;
                       return false;
                   }
                   keepHitBFlag = false;
                   return false;
               }
        }
        else
           return false;
    }
    
}
