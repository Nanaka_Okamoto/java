
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author penguin7
 */
public class Bbullet {
    
    
    
    private Fort app;				// applet
    private Image img;					// picture
    public int x, y;					// position
    private int a;				// angle
    private int b;                           //y = ax+ b
    private boolean appear;			// flag if the Bullet is already on the display

    // constructor
    Bbullet(Fort app) {
        this.app = app;
        appear = false;
//        img = Toolkit.getDefaultToolkit().createImage(Bullet.class.getResource("bullet.gif"));       // read picture file
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\Bbullet.png");               
    }

    // shoot the Bullet
    void shoot(int x, int y, int i)	{
        if (!appear) {
            this.x = x;
            this.y = y;
            
            
            if(i==0){
                a=0;
                b=x;
            }
            else if(i==1){
                a=-4;
                b=y-a*x;
            }
             else if(i==2){
                a=-3;
                b=y-a*x;
            }
             else if(i==3){
                a=-2;
                b=y-a*x;
            }
             else if(i==4){
                a=-1;
                b=y-a*x;
            }         
             else if(i==5){
                a=0;
                b=y;
            }
             else if(i==6){
                a=1;
                b=y-a*x;
            }
             else if(i==7){
                a=2;
                b=y-a*x;
            }
             else if(i==8){
                a=3;
                b=y-a*x;
            }
             else if(i==9){
                a=4;
                b=y-a*x;
            }         
            appear = true;
        }
    }

    // move
    void move(int index) {
        if (appear) {
             x = x+4;
             y= a*x+b;
            if (x < -32 || x > app.getw() || y < -32 || y > app.geth())              //if out of the window
                appear = false;
        }
    }

    // draw the Bullet
    void draw(Graphics g) {
        if (appear&& app.show) g.drawImage(img, (int)x,(int)y, app);
    }
}
