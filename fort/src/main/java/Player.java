/**

 */

import java.awt.*;



public class Player {
    private Fort app;				// applet
    private Image img;					// picture
    private Image imgHit;              //picture when the player is hit
    public int x, y;					// position x,y
    private Bullet bullet;					// Bullet

// constructor
    Player(Fort applet) {
        app = applet;
        x = app.getw() / 2 - 16;            // initialize the position
        y = app.geth() - 85;
//        img = Toolkit.getDefaultToolkit().createImage(Player.class.getResource("player.gif"));                    // read picture file
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\kirby.png");                    // TODO find dynamic solution
        imgHit = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\kirbyhit.png");                    // TODO find dynamic solution
        bullet = new Bullet(app);                               // instantiate Bullet object
    }
	//D:\java project\fort\src\main\pictures\background.png
// movement
    public void move() {
        bullet.move();                   // Bullet movement
        if (Key.left) {                 // in left direction
            x -= 4;
            if (x < 0) x = 0;
        }
        if (Key.right) {                       // right direction
            x += 4;
            if (x > app.getw() - 32) x = app.getw() - 32;
        }
        if (Key.up) {                 // in up direction
            y -= 4;
            if (y < 0) y = 0;
        }
        if (Key.down) {                       // down direction
            y += 4;
            if (y > app.geth() -64 ) y = app.geth() -64 ;
        }
        if (Key.space){
            bullet.shoot(x + 12, y, 0, -8);
        }              // shoot the Bullet
    }

// draw the ship
    public void draw(Graphics g) {
        bullet.draw(g);         //bullet is secretly create here
        
        if(app.level != 6){
            if(app.getHitFlagPlayer(0)||app.getHitFlagPlayer(1)||app.getHitFlagPlayer(2) ||
                    app.getHitFlagPlayer(3)||app.getHitFlagPlayer(4))  {
                g.drawImage(imgHit, x, y, app);
            }
            else{             
                g.drawImage(img, x, y, app);
            }
        }
        else{        
            if( app.getHitFlagPlayer_Boss() || app.getHitFlagPlayer_BBllet(0)|| 
                    app.getHitFlagPlayer_BBllet(1)||app.getHitFlagPlayer_BBllet(2)||
                    app.getHitFlagPlayer_BBllet(3)||app.getHitFlagPlayer_BBllet(4)||
                    app.getHitFlagPlayer_BBllet(5)||app.getHitFlagPlayer_BBllet(6)||
                    app.getHitFlagPlayer_BBllet(7)||app.getHitFlagPlayer_BBllet(8)||
                    app.getHitFlagPlayer_BBllet(9) ){
                g.drawImage(imgHit, x, y, app);
            }
            else{
                g.drawImage(img, x, y, app);
            }
        }    
    
    }

//start !!
    public   void run(Graphics g){
        if(app.show) {//if it is not game-over
            this.move();
            this.draw(g);
        if(app.getHitFlagBoss() || app.getHitFlagEnemy(0) || app.getHitFlagEnemy(1) 
                || app.getHitFlagEnemy(2) || app.getHitFlagEnemy(3) || app.getHitFlagEnemy(4) 
                || app.getHitFlagEnemy2(0) || app.getHitFlagEnemy2(1) || app.getHitFlagEnemy2(2) || 
                app.getHitFlagEnemy2(3) || app.getHitFlagEnemy2(4)){
             bullet.appear=false;
                   
        }
        }
    }

    // it returns the vertical position of bullet
    public int getXbullet(){
        return   bullet.x;
    }

    // it returns the horizontal position of bullet
    public int getYbullet(){
        return bullet.y;
    }

}
