
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author penguin7
 */
public class Enemy2 {
    private Fort app;				// applet
    private Image img;					// image
    private Image effect;            //explosion animation
   public int x, y;					// position
   private int ex, ey;              //position of explosion effect
    private int index;             //index of the enemy array;
    private boolean left;          //flag to tell which side it appears

    // constructor
    public Enemy2(Fort applet, int index) {
        app = applet;
        this.index = index;
        left= true;
        y = (int)(Math.random() * (app.geth() + 32));                       // initialize the position
        x = -32;
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\enemy.gif");
        effect = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\explosion.gif");
    }

    // move
    public void move() {
           //bullet.move();
           if (y >= 100){
                //if(( x <110 && x >90) ||( x>190 && x <210) )
                // bullet.shoot(x, y);
            }
 

        if(app.show){
            if(left){//it appears from left so
                x+=2;// position going right
            }
            else{
                 x-=2;  // position going left
            }
        }           
        if ((left && x > app.getw()) || (!left && x < -32) || !app.show || app.scene != 1) {// if it is outside the frame or not gaming 
            y = (int)(Math.random() * (app.geth() + 32));
            if(left){  //left to right was used so now the position is initialized to start from right side
                x = app.getw() + 32;
                left = false;
            }
            else{//it was left to right so position is initialized to start from left side
                x = -32;
                left = true;
            }
           
        }


    }

    // draw the enemy
    public void draw(Graphics g) {
            if(app.getHitFlagEnemy2(index))  {
               if(app.timerEnemy2Hit[index] == 0){
                      ex = x;
                      ey = y;
                }
                g.drawImage(effect, ex, ey, app);                   // TODO fix the problem of not showing clearly enough
                y = (int)(Math.random() * (app.getw() - 32));
                x = -32;
            }
            else {
                g.drawImage(img, x, y, app);    
             }
    }

    //enemy start !!
    public  void run(Graphics g) {
        if(app.show)   {
            this.move();
            this.draw(g);
        }
    }



}
