/**
 * Created with IntelliJ IDEA.
 * User: Hristo Dimitrov
 * Date: 10/15/12
 * Time: 2:33 AM
 * University Java Project - Fort!
 * Group members: Hristo Dimitrov, Nana Okamoto, Paul Clydesdale, Danne Connolly;
 */
import java.awt.*;

public class Bullet {
    private Fort app;				// applet
    private Image img;					// picture
    public int x, y;					// position
    private int vx, vy;				// speed
    public boolean appear;			// flag if the Bullet is already on the display

// constructor
    public Bullet(Fort applet) {
        app = applet;
        appear = false;
//        img = Toolkit.getDefaultToolkit().createImage(Bullet.class.getResource("bullet.gif"));                            // read picture file
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\bullet.gif");                          // TODO find dynamic solution
    }

// shoot the Bullet
    void shoot(int x, int y, int vx, int vy)	{
        
        if (!appear) {
            this.x = x;
            this.y = y;
            this.vx = vx;
            this.vy = vy;
            appear = true;
        }

    }

// move
    void move() {
        if (appear) {
            x += vx;
            y += vy;
            if (x < -8 || x > app.getw()  || y < 0 || y > app.geth()  ){  //if out of the window
               // x = 0;
               // y= 0;
                appear = false;
            }
            /* if(app.getHitFlagBoss() || app.getHitFlagEnemy(0) || app.getHitFlagEnemy(1) 
                || app.getHitFlagEnemy(2) || app.getHitFlagEnemy(3) || app.getHitFlagEnemy(4) 
                || app.getHitFlagEnemy2(0) || app.getHitFlagEnemy2(1) || app.getHitFlagEnemy2(2) || 
                app.getHitFlagEnemy2(3) || app.getHitFlagEnemy2(4)){
                x = 0;
                y = 0;
                appear = false;      
            }
        }   if(app.scene != 1 ){
                x = 0;
                y = 0;
                appear = false;  
        }*/
        }
    }

// draw the Bullet
    void draw(Graphics g) {
        if (appear && app.show){
            g.drawImage(img, x, y, app);
        }
    }
}
