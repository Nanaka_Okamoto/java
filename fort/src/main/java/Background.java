/**
 * Created with IntelliJ IDEA.
 * User: Hristo Dimitrov
 * Date: 10/15/12
 * Time: 2:34 AM
 * University Java Project - Fort!
 * Group members: Hristo Dimitrov, Nana Okamoto, Paul Clydesdale, Danne Connolly;
 */
import java.awt.*;

public class Background {
    private Fort app;				// jpanel
    private Image img;					// picture
    private Image img2;
    private Image img0;
    private Image img3;
    private Image img4;
    private Image img5;
    private int x, y;   // position x,y

//constructor
    Background(Fort applet){
        app = applet;
        x=y=0;        //initialize the position

        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\rainbow.png");                            // TODO find dynamic solution
        img2 = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\space.png");
        img0 =  Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\start.png");
        img3 =  Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\pattern3.png");
        img4 =  Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\pattern2.png");
        img5 =  Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\pattern7.png");
    }

// draw the background
    public void draw(Graphics g) {

        if(app.scene == 0)  {
            g.drawImage(img0, 0, 0, app);
        }
        else if(app.scene >= 1 &&  app.level == 1)  {
            g.drawImage(img, x, y, app);
            g.drawImage(img, x, y-400, app);
        }
        else if(app.level == 2 )  {
            g.drawImage(img2, x, y, app);
            g.drawImage(img2, x, y-400, app);
        }
         else if(app.level == 3 )  {
            g.drawImage(img3, x, y, app);
            g.drawImage(img3, x, y-400, app);
        }
         else if(app.level == 4 )  {
            g.drawImage(img4, x, y, app);
            g.drawImage(img4, x, y-400, app);
        }
         else if(app.level == 5 )  {
            g.drawImage(img5, x, y, app);
            g.drawImage(img5, x, y-400, app);
        }
          
        else{
            g.drawImage(img2, x, y, app);
            g.drawImage(img2, x, y-400, app);
        }


        if(y==400)  y=0;                     //if scrolled to the bottom bring it back to the original location
    }

//move
    public void move(){
        y+=2;
    }

    public  void run(Graphics g){
        this.draw(g);
        this.move();
    }
}
