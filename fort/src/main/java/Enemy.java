/**
 * Created with IntelliJ IDEA.
 * User: Hristo Dimitrov
 * Date: 10/15/12
 * Time: 2:26 AM
 * University Java Project - Fort!
 * Group members: Hristo Dimitrov, Nana Okamoto, Paul Clydesdale, Danne Connolly;
 */
import java.awt.*;

public class Enemy {
    private Fort app;				// applet
    private Image img;					// image
    private Image effect;            //explosion animation
   public int x, y;					// position
   private int ex, ey;              //position of explosion effect
    private Ebullet bullet;
    private int index;             //index of the enemy array;

    // constructor
    public Enemy(Fort applet, int index) {
        app = applet;
        this.index = index;
        x = (int)(Math.random() * (app.getw() - 32));                       // initialize the position
        y = -32;
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\enemy.gif");
        effect = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\explosion.gif");
        bullet = new Ebullet(app);                               // instantiate Bullet object
    }

    // move
    public void move() {
        if(index== 2 || index == 3) {
            bullet.move();
            if (y ==  100){
                if(x < 150)
                 bullet.shoot(x + 12, y, 2, -8);
                else
                  bullet.shoot(x + 12, y, -2, -8);
            }
        }

        if(app.show){
            if(index == 1 || index == 3 ){
                y+=5;
            }
            else{
                 y += 4;  // position is down
            }
        }           
        if (y > app.geth() || !app.show || app.scene != 1) {// if it is outside the frame or not gaming 
            x = (int)(Math.random() * (app.getw() - 32));
            y = -32;
        }


    }

    // draw the enemy
    public void draw(Graphics g) {
        if(index== 2 || index == 3){
            bullet.draw(g);
        }
            if(app.getHitFlagEnemy(index))  {
               if(app.timerEnemyHit[index] == 0){
                      ex = x;
                      ey = y;
                }
                g.drawImage(effect, ex, ey, app);                   // TODO fix the problem of not showing clearly enough
                x = (int)(Math.random() * (app.getw() - 32));
                y = -32;
            }
            else {
                g.drawImage(img, x, y, app);    
             }
    }

    //enemy start !!
    public  void run(Graphics g) {
        if(app.show)   {
            this.move();
            this.draw(g);
        }
    }



    // it returns the vertical position of bullet
    public int getXbullet(){
        return   bullet.x;
    }

    // it returns the horizontal position of bullet
    public int getYbullet(){
        return bullet.y;
    }

}
