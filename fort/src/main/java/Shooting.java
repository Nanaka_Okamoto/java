/**
 * Created with IntelliJ IDEA.
 * User: Hristo Dimitrov
 * Date: 10/15/12
 * Time: 2:42 AM
 * University Java Project - Fort!
 * Group members: Hristo Dimitrov, Nana Okamoto, Paul Clydesdale, Danne Connolly;
 */
import javax.swing.*;

public class Shooting {
    public static void main(final String[] args) {
        JFrame frame = new JFrame("Rainbow Shooter");
        Fort test = new Fort();
        test.setSize(300, 400);
        frame.getContentPane().add(test);
        frame.setBounds(0 , 0 , 300 , 400);
        frame.setVisible(true);
        test.init();
        test.start();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
