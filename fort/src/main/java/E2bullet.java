
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author penguin7
 */
public class E2bullet {
        private Fort app;				// applet
    private Image img;					// picture
    public int x, y;					// position
    private int vx, vy;				// speed
    private boolean appear;			// flag if the Bullet is already on the display

    // constructor
    public E2bullet(Fort applet) {
        app = applet;
        appear = false;
//        img = Toolkit.getDefaultToolkit().createImage(Bullet.class.getResource("bullet.gif"));                            // read picture file
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\bullet.png");                          // TODO find dynamic solution
    }

    // shoot the Bullet
    void shoot(int x, int y)	{
        if (!appear) {
            this.x = x;
            this.y = y;
           // this.vx = vx;
           // this.vy = vy;
            appear = true;
        }
    }

    // move
    void move() {
        if (appear) {
            
            y = y + 8;
            if (x < -8 || x > app.getw() || y < -8 || y > app.geth())              //if out of the window
                appear = false;
        }
    }

    // draw the Bullet
    void draw(Graphics g) {
        if (appear&& app.show) g.drawImage(img, x, y, app);
    }
}
