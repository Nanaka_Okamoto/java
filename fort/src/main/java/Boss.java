
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.lang.Math;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author penguin7
 */
public class Boss {
    private Fort app;				// applet
    private Image img;					// image
    private Image effect;            //explosion animation
    private Image imgHit;           //img used for boss got hit
    public int x, y;					// position
    private int ex, ey;              //position of explosion effect
    private Bbullet[] bullet = new Bbullet[10];
    public int pattern;             //pattern how the boss 
     boolean down;                  //flag used to tell if boss should go down or now
     boolean bossDied;                 //flag to tell the boss to dead
     boolean left;               //flag to tell if the boss goes left
     
     
    
    
    
    // constructor
    public Boss(Fort applet) {
        app = applet;
        x = app.getw() /2 -64 ;                    // initialize the position
        y = 0 ; 
        img = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\boss.png");
        effect = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\explosion.gif");
        imgHit = Toolkit.getDefaultToolkit().createImage("D:\\java project\\fort\\src\\main\\pictures\\bossHit.png");
        for(int i = 0; i <10; i++){
            bullet[i] = new Bbullet(app); // instantiate Bullet object
        }
        
        pattern = 1;    //initialize the move pattern 
        down = true;
        bossDied = false; 
        left = true;
        
    }

    // move
    public void move() {
        
        if(app.score <=500){
            pattern =1;
        }
        else{
            pattern =2;
        }
       if(pattern == 1) {
            for(int i = 0; i <10; i++){
                bullet[i].move(i);// let the position of bullet determined
            }          
              for(int i = 0; i<10; i++){
                    bullet[i].shoot(x,y, i);
                        
              }
            if(down){// if going down 
                if(y>=0 && y <= 300){
                    y = y+3;    
                }
                else{
                    y = y-3;
                     down = false;
                }
            }
             else  {
                if(y >= 0 && y <= 300){
                    y= y- 3;
                }
                 else {
                    y = y+3;
                      down = true;
                 }     
            }
             
    
        }//end of pattern 1
      
        if(pattern == 2) {
            for(int i = 0; i <10; i++){
                bullet[i].move(i);// let the position of bullet determined
            }
            for(int i = 0; i<10; i++){
                    bullet[i].shoot(x,y, i);       
              }
            
            if(left){// if going left 
                if(x>=0 && x <= 180){
                    x = x-3;    
                }
                else{
                    x = x+3;
                     left = false;
                }
            }
             else  {//going right
                if(x >= 0 && x <180){
                    x= x+3;
                } 
                 else{
                     x = x-3;
                     left = true;
                 }
            }
             
        }//end of pattern 2

    }

    // draw the enemy
    public void draw(Graphics g) {
        for(int i = 0; i < 10; i++){ 
            //first draw bullets
              bullet[i].draw(g);
        }
        //second draw boss based on hit or not hit status
        if(app.getHitFlagBoss()){  
            g.drawImage(imgHit, x, y, app);    
        }
        else if(bossDied){
            for(int e = 0; e< 100; e+=10){
             g.drawImage(effect, x,y, app);
             g.drawImage(effect, (x+e),y, app);
             g.drawImage(effect, (x+e),(y+e), app);
            }
        }
        else{            
            g.drawImage(img, x, y, app);  
        }
             
      
    }

    //enemy start !!
    public  void run(Graphics g) {
        if(app.show)   {
            this.move();
            this.draw(g);
        }
    }


    // it returns the horizontal position of bullet
    public int getYbullet(int index){
      return bullet[index].y;
    }  
 // it returns the vertical position of bullet
   public int getXbullet(int index) {
        return   bullet[index].x;
    }
}
