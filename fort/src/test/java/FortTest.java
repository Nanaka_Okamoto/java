import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
* Created with IntelliJ IDEA.
* User: Hristo Dimitrov
* Date: 10/15/12
* Time: 2:50 AM
* University Java Project - Fort!
* Group members: Hristo Dimitrov, Nana Okamoto, Paul Clydesdale, Danne Connolly;
*/

public class FortTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testInit() throws Exception {

    }

    @Test
    public void testStart() throws Exception {

    }

    @Test
    public void testStop() throws Exception {

    }

    @Test
    public void testRun() throws Exception {

    }

    @Test
    public void testPaintComponent() throws Exception {

    }

    @Test
    public void testGetUI() throws Exception {

    }

    @Test
    public void testSetUI() throws Exception {

    }

    @Test
    public void testGetAccessibleContext() throws Exception {

    }

    @Test
    public void testSetInheritsPopupMenu() throws Exception {

    }

    @Test
    public void testGetInheritsPopupMenu() throws Exception {

    }

    @Test
    public void testSetComponentPopupMenu() throws Exception {

    }

    @Test
    public void testGetComponentPopupMenu() throws Exception {

    }

    @Test
    public void testUpdateUI() throws Exception {

    }

    @Test
    public void testGetUIClassID() throws Exception {

    }

    @Test
    public void testUpdate() throws Exception {

    }

    @Test
    public void testPaint() throws Exception {

    }

    @Test
    public void testPrintAll() throws Exception {

    }

    @Test
    public void testPrint() throws Exception {

    }

    @Test
    public void testIsPaintingTile() throws Exception {

    }

    @Test
    public void testIsPaintingForPrint() throws Exception {

    }

    @Test
    public void testIsManagingFocus() throws Exception {

    }

    @Test
    public void testSetNextFocusableComponent() throws Exception {

    }

    @Test
    public void testGetNextFocusableComponent() throws Exception {

    }

    @Test
    public void testSetRequestFocusEnabled() throws Exception {

    }

    @Test
    public void testIsRequestFocusEnabled() throws Exception {

    }

    @Test
    public void testRequestFocus() throws Exception {

    }

    @Test
    public void testRequestFocusInWindow() throws Exception {

    }

    @Test
    public void testGrabFocus() throws Exception {

    }

    @Test
    public void testSetVerifyInputWhenFocusTarget() throws Exception {

    }

    @Test
    public void testGetVerifyInputWhenFocusTarget() throws Exception {

    }

    @Test
    public void testGetFontMetrics() throws Exception {

    }

    @Test
    public void testSetPreferredSize() throws Exception {

    }

    @Test
    public void testGetPreferredSize() throws Exception {

    }

    @Test
    public void testSetMaximumSize() throws Exception {

    }

    @Test
    public void testGetMaximumSize() throws Exception {

    }

    @Test
    public void testSetMinimumSize() throws Exception {

    }

    @Test
    public void testGetMinimumSize() throws Exception {

    }

    @Test
    public void testContains() throws Exception {

    }

    @Test
    public void testSetBorder() throws Exception {

    }

    @Test
    public void testGetBorder() throws Exception {

    }

    @Test
    public void testGetInsets() throws Exception {

    }

    @Test
    public void testGetAlignmentY() throws Exception {

    }

    @Test
    public void testSetAlignmentY() throws Exception {

    }

    @Test
    public void testGetAlignmentX() throws Exception {

    }

    @Test
    public void testSetAlignmentX() throws Exception {

    }

    @Test
    public void testSetInputVerifier() throws Exception {

    }

    @Test
    public void testGetInputVerifier() throws Exception {

    }

    @Test
    public void testGetGraphics() throws Exception {

    }

    @Test
    public void testSetDebugGraphicsOptions() throws Exception {

    }

    @Test
    public void testGetDebugGraphicsOptions() throws Exception {

    }

    @Test
    public void testRegisterKeyboardAction() throws Exception {

    }

    @Test
    public void testUnregisterKeyboardAction() throws Exception {

    }

    @Test
    public void testGetRegisteredKeyStrokes() throws Exception {

    }

    @Test
    public void testGetConditionForKeyStroke() throws Exception {

    }

    @Test
    public void testGetActionForKeyStroke() throws Exception {

    }

    @Test
    public void testResetKeyboardActions() throws Exception {

    }

    @Test
    public void testSetInputMap() throws Exception {

    }

    @Test
    public void testGetInputMap() throws Exception {

    }

    @Test
    public void testSetActionMap() throws Exception {

    }

    @Test
    public void testGetActionMap() throws Exception {

    }

    @Test
    public void testGetBaseline() throws Exception {

    }

    @Test
    public void testGetBaselineResizeBehavior() throws Exception {

    }

    @Test
    public void testRequestDefaultFocus() throws Exception {

    }

    @Test
    public void testSetVisible() throws Exception {

    }

    @Test
    public void testSetEnabled() throws Exception {

    }

    @Test
    public void testSetForeground() throws Exception {

    }

    @Test
    public void testSetBackground() throws Exception {

    }

    @Test
    public void testSetFont() throws Exception {

    }

    @Test
    public void testGetDefaultLocale() throws Exception {

    }

    @Test
    public void testSetDefaultLocale() throws Exception {

    }

    @Test
    public void testSetToolTipText() throws Exception {

    }

    @Test
    public void testGetToolTipText() throws Exception {

    }

    @Test
    public void testGetToolTipLocation() throws Exception {

    }

    @Test
    public void testGetPopupLocation() throws Exception {

    }

    @Test
    public void testCreateToolTip() throws Exception {

    }

    @Test
    public void testScrollRectToVisible() throws Exception {

    }

    @Test
    public void testSetAutoscrolls() throws Exception {

    }

    @Test
    public void testGetAutoscrolls() throws Exception {

    }

    @Test
    public void testSetTransferHandler() throws Exception {

    }

    @Test
    public void testGetTransferHandler() throws Exception {

    }

    @Test
    public void testEnable() throws Exception {

    }

    @Test
    public void testDisable() throws Exception {

    }

    @Test
    public void testGetClientProperty() throws Exception {

    }

    @Test
    public void testPutClientProperty() throws Exception {

    }

    @Test
    public void testSetFocusTraversalKeys() throws Exception {

    }

    @Test
    public void testIsLightweightComponent() throws Exception {

    }

    @Test
    public void testReshape() throws Exception {

    }

    @Test
    public void testGetBounds() throws Exception {

    }

    @Test
    public void testGetSize() throws Exception {

    }

    @Test
    public void testGetLocation() throws Exception {

    }

    @Test
    public void testGetX() throws Exception {

    }

    @Test
    public void testGetY() throws Exception {

    }

    @Test
    public void testGetWidth() throws Exception {

    }

    @Test
    public void testGetHeight() throws Exception {

    }

    @Test
    public void testIsOpaque() throws Exception {

    }

    @Test
    public void testSetOpaque() throws Exception {

    }

    @Test
    public void testComputeVisibleRect() throws Exception {

    }

    @Test
    public void testGetVisibleRect() throws Exception {

    }

    @Test
    public void testFirePropertyChange() throws Exception {

    }

    @Test
    public void testAddVetoableChangeListener() throws Exception {

    }

    @Test
    public void testRemoveVetoableChangeListener() throws Exception {

    }

    @Test
    public void testGetVetoableChangeListeners() throws Exception {

    }

    @Test
    public void testGetTopLevelAncestor() throws Exception {

    }

    @Test
    public void testAddAncestorListener() throws Exception {

    }

    @Test
    public void testRemoveAncestorListener() throws Exception {

    }

    @Test
    public void testGetAncestorListeners() throws Exception {

    }

    @Test
    public void testGetListeners() throws Exception {

    }

    @Test
    public void testAddNotify() throws Exception {

    }

    @Test
    public void testRemoveNotify() throws Exception {

    }

    @Test
    public void testRepaint() throws Exception {

    }

    @Test
    public void testRevalidate() throws Exception {

    }

    @Test
    public void testIsValidateRoot() throws Exception {

    }

    @Test
    public void testIsOptimizedDrawingEnabled() throws Exception {

    }

    @Test
    public void testPaintImmediately() throws Exception {

    }

    @Test
    public void testSetDoubleBuffered() throws Exception {

    }

    @Test
    public void testIsDoubleBuffered() throws Exception {

    }

    @Test
    public void testGetRootPane() throws Exception {

    }

    @Test
    public void testGetComponentCount() throws Exception {

    }

    @Test
    public void testCountComponents() throws Exception {

    }

    @Test
    public void testGetComponent() throws Exception {

    }

    @Test
    public void testGetComponents() throws Exception {

    }

    @Test
    public void testInsets() throws Exception {

    }

    @Test
    public void testAdd() throws Exception {

    }

    @Test
    public void testSetComponentZOrder() throws Exception {

    }

    @Test
    public void testGetComponentZOrder() throws Exception {

    }

    @Test
    public void testRemove() throws Exception {

    }

    @Test
    public void testRemoveAll() throws Exception {

    }

    @Test
    public void testGetLayout() throws Exception {

    }

    @Test
    public void testSetLayout() throws Exception {

    }

    @Test
    public void testDoLayout() throws Exception {

    }

    @Test
    public void testLayout() throws Exception {

    }

    @Test
    public void testInvalidate() throws Exception {

    }

    @Test
    public void testValidate() throws Exception {

    }

    @Test
    public void testPreferredSize() throws Exception {

    }

    @Test
    public void testMinimumSize() throws Exception {

    }

    @Test
    public void testPaintComponents() throws Exception {

    }

    @Test
    public void testPrintComponents() throws Exception {

    }

    @Test
    public void testAddContainerListener() throws Exception {

    }

    @Test
    public void testRemoveContainerListener() throws Exception {

    }

    @Test
    public void testGetContainerListeners() throws Exception {

    }

    @Test
    public void testDeliverEvent() throws Exception {

    }

    @Test
    public void testGetComponentAt() throws Exception {

    }

    @Test
    public void testLocate() throws Exception {

    }

    @Test
    public void testGetMousePosition() throws Exception {

    }

    @Test
    public void testFindComponentAt() throws Exception {

    }

    @Test
    public void testIsAncestorOf() throws Exception {

    }

    @Test
    public void testList() throws Exception {

    }

    @Test
    public void testGetFocusTraversalKeys() throws Exception {

    }

    @Test
    public void testAreFocusTraversalKeysSet() throws Exception {

    }

    @Test
    public void testIsFocusCycleRoot() throws Exception {

    }

    @Test
    public void testTransferFocusBackward() throws Exception {

    }

    @Test
    public void testSetFocusTraversalPolicy() throws Exception {

    }

    @Test
    public void testGetFocusTraversalPolicy() throws Exception {

    }

    @Test
    public void testIsFocusTraversalPolicySet() throws Exception {

    }

    @Test
    public void testSetFocusCycleRoot() throws Exception {

    }

    @Test
    public void testSetFocusTraversalPolicyProvider() throws Exception {

    }

    @Test
    public void testIsFocusTraversalPolicyProvider() throws Exception {

    }

    @Test
    public void testTransferFocusDownCycle() throws Exception {

    }

    @Test
    public void testApplyComponentOrientation() throws Exception {

    }

    @Test
    public void testAddPropertyChangeListener() throws Exception {

    }

    @Test
    public void testGetName() throws Exception {

    }

    @Test
    public void testSetName() throws Exception {

    }

    @Test
    public void testGetParent() throws Exception {

    }

    @Test
    public void testGetPeer() throws Exception {

    }

    @Test
    public void testSetDropTarget() throws Exception {

    }

    @Test
    public void testGetDropTarget() throws Exception {

    }

    @Test
    public void testGetGraphicsConfiguration() throws Exception {

    }

    @Test
    public void testGetTreeLock() throws Exception {

    }

    @Test
    public void testGetToolkit() throws Exception {

    }

    @Test
    public void testIsValid() throws Exception {

    }

    @Test
    public void testIsDisplayable() throws Exception {

    }

    @Test
    public void testIsVisible() throws Exception {

    }


    @Test
    public void testIsShowing() throws Exception {

    }

    @Test
    public void testIsEnabled() throws Exception {

    }

    @Test
    public void testEnableInputMethods() throws Exception {

    }

    @Test
    public void testShow() throws Exception {

    }

    @Test
    public void testHide() throws Exception {

    }

    @Test
    public void testGetForeground() throws Exception {

    }

    @Test
    public void testIsForegroundSet() throws Exception {

    }

    @Test
    public void testGetBackground() throws Exception {

    }

    @Test
    public void testIsBackgroundSet() throws Exception {

    }

    @Test
    public void testGetFont() throws Exception {

    }

    @Test
    public void testIsFontSet() throws Exception {

    }

    @Test
    public void testGetLocale() throws Exception {

    }

    @Test
    public void testSetLocale() throws Exception {

    }

    @Test
    public void testGetColorModel() throws Exception {

    }

    @Test
    public void testGetLocationOnScreen() throws Exception {

    }

    @Test
    public void testLocation() throws Exception {

    }

    @Test
    public void testSetLocation() throws Exception {

    }

    @Test
    public void testMove() throws Exception {

    }

    @Test
    public void testSize() throws Exception {

    }

    @Test
    public void testSetSize() throws Exception {

    }

    @Test
    public void testResize() throws Exception {

    }

    @Test
    public void testBounds() throws Exception {

    }

    @Test
    public void testSetBounds() throws Exception {

    }

    @Test
    public void testIsLightweight() throws Exception {

    }

    @Test
    public void testIsPreferredSizeSet() throws Exception {

    }

    @Test
    public void testIsMinimumSizeSet() throws Exception {

    }

    @Test
    public void testIsMaximumSizeSet() throws Exception {

    }

    @Test
    public void testSetCursor() throws Exception {

    }

    @Test
    public void testGetCursor() throws Exception {

    }

    @Test
    public void testIsCursorSet() throws Exception {

    }

    @Test
    public void testPaintAll() throws Exception {

    }

    @Test
    public void testImageUpdate() throws Exception {

    }

    @Test
    public void testCreateImage() throws Exception {

    }

    @Test
    public void testCreateVolatileImage() throws Exception {

    }

    @Test
    public void testPrepareImage() throws Exception {

    }

    @Test
    public void testCheckImage() throws Exception {

    }

    @Test
    public void testSetIgnoreRepaint() throws Exception {

    }

    @Test
    public void testGetIgnoreRepaint() throws Exception {

    }

    @Test
    public void testInside() throws Exception {

    }

    @Test
    public void testDispatchEvent() throws Exception {

    }

    @Test
    public void testPostEvent() throws Exception {

    }

    @Test
    public void testAddComponentListener() throws Exception {

    }

    @Test
    public void testRemoveComponentListener() throws Exception {

    }

    @Test
    public void testGetComponentListeners() throws Exception {

    }

    @Test
    public void testAddFocusListener() throws Exception {

    }

    @Test
    public void testRemoveFocusListener() throws Exception {

    }

    @Test
    public void testGetFocusListeners() throws Exception {

    }

    @Test
    public void testAddHierarchyListener() throws Exception {

    }

    @Test
    public void testRemoveHierarchyListener() throws Exception {

    }

    @Test
    public void testGetHierarchyListeners() throws Exception {

    }

    @Test
    public void testAddHierarchyBoundsListener() throws Exception {

    }

    @Test
    public void testRemoveHierarchyBoundsListener() throws Exception {

    }

    @Test
    public void testGetHierarchyBoundsListeners() throws Exception {

    }

    @Test
    public void testAddKeyListener() throws Exception {

    }

    @Test
    public void testRemoveKeyListener() throws Exception {

    }

    @Test
    public void testGetKeyListeners() throws Exception {

    }

    @Test
    public void testAddMouseListener() throws Exception {

    }

    @Test
    public void testRemoveMouseListener() throws Exception {

    }

    @Test
    public void testGetMouseListeners() throws Exception {

    }

    @Test
    public void testAddMouseMotionListener() throws Exception {

    }

    @Test
    public void testRemoveMouseMotionListener() throws Exception {

    }

    @Test
    public void testGetMouseMotionListeners() throws Exception {

    }

    @Test
    public void testRemoveMouseWheelListener() throws Exception {

    }

    @Test
    public void testGetMouseWheelListeners() throws Exception {

    }

    @Test
    public void testAddInputMethodListener() throws Exception {

    }

    @Test
    public void testRemoveInputMethodListener() throws Exception {

    }

    @Test
    public void testGetInputMethodListeners() throws Exception {

    }

    @Test
    public void testGetInputMethodRequests() throws Exception {

    }

    @Test
    public void testGetInputContext() throws Exception {

    }

    @Test
    public void testHandleEvent() throws Exception {

    }

    @Test
    public void testMouseDown() throws Exception {

    }

    @Test
    public void testMouseDrag() throws Exception {

    }

    @Test
    public void testMouseUp() throws Exception {

    }

    @Test
    public void testMouseMove() throws Exception {

    }

    @Test
    public void testMouseEnter() throws Exception {

    }

    @Test
    public void testMouseExit() throws Exception {

    }

    @Test
    public void testKeyDown() throws Exception {

    }

    @Test
    public void testKeyUp() throws Exception {

    }

    @Test
    public void testAction() throws Exception {

    }

    @Test
    public void testGotFocus() throws Exception {

    }

    @Test
    public void testLostFocus() throws Exception {

    }

    @Test
    public void testIsFocusTraversable() throws Exception {

    }

    @Test
    public void testIsFocusable() throws Exception {

    }

    @Test
    public void testSetFocusable() throws Exception {

    }

    @Test
    public void testSetFocusTraversalKeysEnabled() throws Exception {

    }

    @Test
    public void testGetFocusTraversalKeysEnabled() throws Exception {

    }

    @Test
    public void testTransferFocus() throws Exception {

    }

    @Test
    public void testGetFocusCycleRootAncestor() throws Exception {

    }

    @Test
    public void testNextFocus() throws Exception {

    }

    @Test
    public void testTransferFocusUpCycle() throws Exception {

    }

    @Test
    public void testHasFocus() throws Exception {

    }

    @Test
    public void testIsFocusOwner() throws Exception {

    }

    @Test
    public void testToString() throws Exception {

    }

    @Test
    public void testRemovePropertyChangeListener() throws Exception {

    }

    @Test
    public void testGetPropertyChangeListeners() throws Exception {

    }

    @Test
    public void testSetComponentOrientation() throws Exception {

    }

    @Test
    public void testGetComponentOrientation() throws Exception {

    }
}
